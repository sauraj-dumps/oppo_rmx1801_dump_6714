## aosp_RMX1801-userdebug 12 SQ1D.211205.016.A1 1641098910 test-keys
- Manufacturer: oppo
- Platform: sdm660
- Codename: RMX1801
- Brand: oppo
- Flavor: aosp_RMX1801-userdebug
- Release Version: 12
- Id: SQ1D.211205.016.A1
- Incremental: 1641098910
- Tags: test-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: oppo/RMX1801/RMX1801:12/SQ1D.211205.016.A1/0448:userdebug/release-keys
- OTA version: 
- Branch: aosp_RMX1801-userdebug-12-SQ1D.211205.016.A1-1641098910-test-keys
- Repo: oppo_rmx1801_dump_6714


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
